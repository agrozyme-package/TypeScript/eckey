## Elliptic Curve Key CLI

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->

# Usage

<!-- usage -->
```sh-session
$ npm install -g @agrozyme/eckey
$ eckey COMMAND
running command...
$ eckey (--version)
@agrozyme/eckey/0.0.4 win32-x64 node-v22.12.0
$ eckey --help [COMMAND]
USAGE
  $ eckey COMMAND
...
```
<!-- usagestop -->

# Commands

<!-- commands -->
* [`eckey convert`](#eckey-convert)
* [`eckey help [COMMAND]`](#eckey-help-command)
* [`eckey make`](#eckey-make)

## `eckey convert`

convert string to other format

```
USAGE
  $ eckey convert --data <value> [--from
    base16|base32|base32crockford|base32hex|base58|base58check|base58flickr|base58xmr|base58xrp|base64|base64url|base64u
    rlnopad|hex|utf8] [--to base16|base32|base32crockford|base32hex|base58|base58check|base58flickr|base58xmr|base58xrp|
    base64|base64url|base64urlnopad|hex|utf8]

FLAGS
  --data=<value>   (required)
  --from=<option>  [default: hex]
                   <options: base16|base32|base32crockford|base32hex|base58|base58check|base58flickr|base58xmr|base58xrp
                   |base64|base64url|base64urlnopad|hex|utf8>
  --to=<option>    [default: hex]
                   <options: base16|base32|base32crockford|base32hex|base58|base58check|base58flickr|base58xmr|base58xrp
                   |base64|base64url|base64urlnopad|hex|utf8>

DESCRIPTION
  convert string to other format

EXAMPLES
  $ eckey convert
```

_See code: [src/commands/convert.ts](https://gitlab.com/agrozyme-package/TypeScript/eckey/blob/v0.0.4/src/commands/convert.ts)_

## `eckey help [COMMAND]`

Display help for eckey.

```
USAGE
  $ eckey help [COMMAND...] [-n]

ARGUMENTS
  COMMAND...  Command to show help for.

FLAGS
  -n, --nested-commands  Include all nested commands in the output.

DESCRIPTION
  Display help for eckey.
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v6.2.20/src/commands/help.ts)_

## `eckey make`

make elliptic curve key pair

```
USAGE
  $ eckey make --curve
    bls12_381|bn254|ed448|ed448ph|ed25519|ed25519ctx|ed25519ph|jubjub|p256|p384|p521|pallas|schnorr|secp256k1|secp256r1|
    secp384r1|secp521r1|vesta [--format base16|base32|base32crockford|base32hex|base58|base58check|base58flickr|base58xm
    r|base58xrp|base64|base64url|base64urlnopad|hex|utf8] [--count <value>]

FLAGS
  --count=<value>    [default: 1]
  --curve=<option>   (required)
                     <options: bls12_381|bn254|ed448|ed448ph|ed25519|ed25519ctx|ed25519ph|jubjub|p256|p384|p521|pallas|s
                     chnorr|secp256k1|secp256r1|secp384r1|secp521r1|vesta>
  --format=<option>  [default: hex]
                     <options: base16|base32|base32crockford|base32hex|base58|base58check|base58flickr|base58xmr|base58x
                     rp|base64|base64url|base64urlnopad|hex|utf8>

DESCRIPTION
  make elliptic curve key pair

EXAMPLES
  $ eckey make
```

_See code: [src/commands/make.ts](https://gitlab.com/agrozyme-package/TypeScript/eckey/blob/v0.0.4/src/commands/make.ts)_
<!-- commandsstop -->
