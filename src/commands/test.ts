import { curveMap } from '@agrozyme/crypto';
import { Command } from '@oclif/core';

export default class Test extends Command {
  static hidden = true;
  static description = 'test the command here';
  static examples = ['<%= config.bin %> <%= command.id %>'];
  static flags = {};
  static args = {};

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Test);

    const curve = curveMap['ed25519'];
    const privateKey = curve.randomPrivateKey();
    const publicKey = curve.getPublicKey(privateKey, false);
    console.dir(publicKey.length);

    //    const data: Record<string, number> = {};
    //    Object.keys(curveMap).forEach((name) => {
    //      const curve = curveMap[name];
    //      data[name] = curve.randomPrivateKey().length;
    //    });
    //
    //    console.dir(data, { depth: 0 });
  }
}
