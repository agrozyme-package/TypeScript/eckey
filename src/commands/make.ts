import { bytesCoderMap, BytesCoderType, curveMap, CurveType } from '@agrozyme/crypto';
import { Command, Flags } from '@oclif/core';
import { bytesCoderKeys, curveKeys } from '../types';

export default class Make extends Command {
  static description = 'make elliptic curve key pair';
  static examples = ['<%= config.bin %> <%= command.id %>'];
  static flags = {
    curve: Flags.string({ required: true, options: curveKeys }),
    format: Flags.string({ options: bytesCoderKeys, default: 'hex' }),
    count: Flags.integer({ min: 1, default: 1 }),
  };
  static args = {};

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Make);
    const curve = curveMap[<CurveType>flags.curve];
    const keyConveter = bytesCoderMap[<BytesCoderType>flags.format];
    const items = [];

    for (let index = 0; index < flags.count; index++) {
      const privateKey = curve.randomPrivateKey();
      const publicKey = curve.getPublicKey(privateKey, true);
      const item = { privateKey: keyConveter.encode(privateKey), publicKey: keyConveter.encode(publicKey) };
      items.push(item);
    }

    console.log(`make ${flags.curve} curve ${flags.count} keypair(s) with ${flags.format} format:`);
    console.dir(items);
  }
}
