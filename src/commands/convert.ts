import { bytesCoderMap, BytesCoderType } from '@agrozyme/crypto';
import { Command, Flags } from '@oclif/core';
import { bytesCoderKeys } from '../types';

export default class Convert extends Command {
  static description = 'convert string to other format';
  static examples = ['<%= config.bin %> <%= command.id %>'];

  static flags = {
    data: Flags.string({ required: true }),
    from: Flags.string({ options: bytesCoderKeys, default: 'hex' }),
    to: Flags.string({ options: bytesCoderKeys, default: 'hex' }),
  };

  static args = {};

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Convert);
    const { data, from, to } = flags;
    const fromConveter = bytesCoderMap[<BytesCoderType>from];
    const toConveter = bytesCoderMap[<BytesCoderType>to];

    const items = fromConveter.decode(data);
    const result = toConveter.encode(items);

    console.info(`Convert data form ${from} to ${to}:`);
    console.dir({ data, result });
  }
}
