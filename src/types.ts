import { bytesCoderMap, curveMap } from '@agrozyme/crypto';
import { keys } from 'rambdax';

export const bytesCoderKeys = keys(bytesCoderMap);
export const curveKeys = keys(curveMap);
